//Enquire Media Queries

var navEl = $(".left, .right");

enquire.register("screen and (max-width: 740px)",{
	match : function() {
		$(navEl).hide();
		$("nav").addClass("navClosed");
	},
	unmatch : function() {
		$(navEl).show();
		$("nav").removeClass("navClosed");
	}
});

$(".menuToggle").click(function() {
	$(navEl).slideToggle();
});

$(window).load(function() {
	$(".portfolioPic").addClass("imgLoaded");
});