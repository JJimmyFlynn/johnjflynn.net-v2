<?php
/*
Template Name: Blog
*/
?>

<?php get_header(); ?>

		<div class="contain-type">
			<?php query_posts( 'posts_per_page=5' );?>
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<article class="blogExcerpt">
				<h2 class="h3"><?php the_title(); ?></h2>
				<p class="published"><time date-time="???"><?php the_date(); ?></time></p>
				<div class="excerpt">
					<?php the_excerpt(); ?>
				</div>
				<a class="readOn" href="<?php the_permalink(); ?>">Read On &rarr;</a>
			</article>
			<?php endwhile; endif; ?>
		</div>

		<?php get_footer(); ?>