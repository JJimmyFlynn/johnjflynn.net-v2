<?php
/*
Template Name: Contact
*/
?>

<?php get_header(); ?>

		<div class="contain-type">
			<div class="contactBlurb">
				<p class="availability"><span>Availability: </span>Currently available for work.</p>
				<p>Have a project that needs doing? Looking for a photo print? Just want to say hello? Use the form below and I'll get back to you in a flash.</p>
			</div>
			<?php the_post(); ?>
			<?php the_content(); ?>
		</div>
		
		<?php include("parts/about-footer.php"); ?>	

		<?php get_footer(); ?>