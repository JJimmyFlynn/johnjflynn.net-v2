	</section> <!-- .main -->
	<script src="<?php bloginfo(template_url); ?>/assets/js/common-ck.js"></script>
	<!--WP Generated Footer -->
	<?php wp_footer(); ?>
	<!--End WP Generated Footer -->
	<!--Google Analytics-->
	<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36782995-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

	</script>
</body>
</html>