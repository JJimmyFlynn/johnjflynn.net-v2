<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>John Flynn</title>
	<meta name="viewport" content="width=device-width">
	<link rel="shortcut icon" href="<?php bloginfo("template_url"); ?>/favicon.ico">
	<link rel="stylesheet" href="<?php bloginfo(template_url); ?>/style.css">
	<!-- Typekit Code -->
	<script type="text/javascript" src="//use.typekit.net/tgz2ezj.js"></script>
	<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
	<!--WP Generated Header -->
	<?php wp_head(); ?>
	<!--End WP Generated Header -->
</head>
<body class="home">
	<section class="topBar">
		<div class="contain">
			<?php include("parts/nav.php"); ?>
		</div>
	</section> <!-- .topBar -->

	<header class="homeHeader" role="banner">
		<div class="contain-type tagline">
			<p>Web Development, photography, and occassional ramblings</p>
			<p class="authorLine">By John Flynn</p>
		</div>
	</header>

	<section class="main" role="main">