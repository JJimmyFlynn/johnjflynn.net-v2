<?php get_header("home"); ?>

		<div class="contain-type">
			<?php query_posts( 'posts_per_page=1' );?>
		  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<article class="entry">
				<h1 class="postTitle"><?php the_title(); ?></h1>
				<p class="published"><time date-time="???"><?php the_date(); ?></time></p>
			  <?php the_content(); ?>
			</article>
		  <?php endwhile; endif; ?>
		  <?php wp_reset_query(); ?>
		</div>

		<?php get_footer(); ?>