		<section class="about">
			<div class="contain">
					<img src="<?php bloginfo(template_url); ?>/assets/img/headshot.jpg" alt="">
					<div class="text">
						<h3>A Bit About John Flynn</h3>
						<p>John Flynn is a front-end developer from northern New England. Though often chained to a computer, when not developing or learning John can be found doing something outside. Far from the internet.</p>
					</div>
			</div>
		</section>