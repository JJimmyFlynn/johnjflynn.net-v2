			<nav role="navigation">
				<span class="left">
					<a href="/blog">Blog</a>
					<a href="/portfolio">Portfolio</a>
					<a href="/skills">Skills</a>
				</span>
					<a href="/" class="logo"><img src="<?php bloginfo(template_url); ?>/assets/img/JF_Logo.svg" alt="John Flynn Logo"></a>
				<span class="right">
					<a href="/photography">Photography</a>
					<a href="/about">About</a>
					<a href="/contact">Contact</a>
				</span>
				<a href="#0" class=" menuToggle" data-icon="m"> Menu</a>
			</nav>