<?php
/*
Template Name: Photography
*/
?>

<?php get_header(); ?>

		<div class="contain">

		<div class="fotorama"
		data-width="100%"
		data-nav="thumbs"
		data-thumb-width="150px"
		data-thumb-height= "100px"
		data-keyboard="true"
		data-transition="crossfade"
		>
			<?php $query = new WP_Query(array(
			'post_type' => 'photography slides'
		)); ?>
	  <?php if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>
			<a href="<?php the_field('image'); ?>"></a>

		  <?php endwhile; endif; ?>
			<?php wp_reset_postdata(); ?>

		</div> <!--end .fotorama -->

		<section class="contain-type entry">
			<p>Disclaimer: These photos are really just here for my own benefit. But if you happen to be interested in a print,
				feel free to <a href="/contact">contact me</a> and I'll be happy to send one your way. </p>
		</section>

		</div>

		<?php get_footer(); ?>