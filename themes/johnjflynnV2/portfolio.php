<?php
/*
Template Name: Portfolio
*/
?>

<?php get_header(); ?>

		<div class="contain">
			<h1 class="portfolioTitle">Selected Work</h1>

			<?php $query = new WP_Query(array(
				'post_type' => 'portfolioItem',
				'show_posts' => -1
			)); ?>
		  <?php if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>
			<article class="portfolioItem">
				<div class="portfolioPic">
			    <div class="loader">
			      <div class="spinner"></div>
			    </div>
					<?php the_post_thumbnail(); ?>
				</div>
				<div class="text">
					<h2 class="h3"><?php the_title(); ?></h2>
					<p class="tech"><?php the_field('tech'); ?></p>
					<p class="portfolioBlurb"><?php the_content(); ?></p>
					<a href="<?php the_field('site_link'); ?>" class="visit">Visit Site &rarr;</a>
				</div>
			</article>

		<?php endwhile; endif; ?>
		<?php wp_reset_query(); ?>
		</div>

		<?php get_footer(); ?>